package wdMethods1;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.*;

import excel.ReadExcel;

public class ProjectMethods extends SeMethods {
	
	@DataProvider(name="fetchData")
	public Object[][] getData() throws IOException {
	Object[][] excelData = ReadExcel.getExcelData("./data/Clead.xlsx");
	return excelData;
	}
	
	@BeforeMethod
	public void step1() {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("Id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("Id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("Class","decorativeSubmit");
		click(eleLogin);
	}
	//@AfterMethod
	public void close()
	{
		closeAllBrowsers();
	}
}
