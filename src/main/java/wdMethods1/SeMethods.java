package wdMethods1;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.sound.midi.SysexMessage;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

public class SeMethods extends ExtentReport implements WdMethods{
	public int i = 1;
	public RemoteWebDriver driver;

	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")){
				System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
	
				ChromeOptions options = new ChromeOptions();
				 
                // Set the experimental option
		options.addArguments("--disable-notifications");
 

				driver = new ChromeDriver(options);
			} else if (browser.equalsIgnoreCase("firefox")){
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			//reportStep("The Browser "+browser+" Launched Successfully", "pass");
		} catch (WebDriverException e) {
			//reportStep("The Browser "+browser+" not Launched", "Fail");

		} finally {
			takeSnap();
		}

	}


	public WebElement locateElement(String locator, String locValue) {
		try {
			switch(locator) {
			case "Id"	 : return driver.findElementById(locValue);
			case "Class" : return driver.findElementByClassName(locValue);
			case "Xpath" : return driver.findElementByXPath(locValue);
			case "LinkText": return driver.findElementByLinkText(locValue);
			}
		} catch (NoSuchElementException e) {
			//reportStep("The Element is not found", "fail");
		} catch (Exception e) {
			//reportStep("Unknown Exception", "fail");
		}
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		// TODO Auto-generated method stub
		return null;
	}


	public void type(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
			//reportStep("The data " +data+ " is Entered Successfully","pass");
			//System.out.println("The data "+data+" is Entered Successfully");
		} catch (WebDriverException e) {
			//reportStep("The data "+data+" is Not Entered", "fail");
			//System.out.println("The data "+data+" is Not Entered");
		} finally {
			takeSnap();
		}
	}


	public void clickWithNoSnap(WebElement ele) {
		try {
			ele.click();
			//reportStep("The Element "+ele+" Clicked Successfully", "Pass");

		} catch (Exception e) {
			//reportStep("The Element "+ele+"is not Clicked","Fail");
		}
	}


	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			//reportStep("The Element "+ele+" Clicked Successfully", "Pass");
		} 
		catch (WebDriverException e) {
			//reportStep("The Element "+ele+"is not Clicked","Fail");
		} finally {
			takeSnap();
		}
	}

	@Override
	public String getText(WebElement ele) {
		// TODO Auto-generated method stub
		String text = null;
		try {
			 text = ele.getText();
			//reportStep("Text "+ text, "pass");
		} catch (NoSuchElementException e) {
			// TODO Auto-generated catch block
			//reportStep("The WebElement is not found unable to read text","Fail");

		}
		return text;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select dd = new Select(ele);
			dd.selectByVisibleText(value);
			//reportStep("The DropDown Is Selected with VisibleText value "+value, "pass");
		} catch (Exception e) {
			//reportStep("The DropDown Is not Selected with VisibleText "+value,"fail");
		} finally {
			takeSnap();
		}

	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		// TODO Auto-generated method stub
		try {
			Select dd1 = new Select(ele);
			dd1.selectByIndex(index);
			//reportStep("The DropDown Is Selected with index value ", "pass");
		} catch (Exception e) {
			//reportStep("The DropDown Is not Selected with index ","fail");
		} finally {
			takeSnap();
		}
	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		// TODO Auto-generated method stub
		try {
			String title = driver.getTitle();
			if(title.contains(expectedTitle))
			{
				System.out.println("Title verified");
			}
			else
			{
				System.out.println("Title doesn't match");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("");
		}
		return false;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		try {
			String text = ele.getText();
			System.out.println(text);
			if(text.equalsIgnoreCase(expectedText))
			{
				System.out.println("Text verified");
			}
			else
			{
				System.err.println("Text doesn't match");
			}
		} catch (NoSuchElementException e) {
			// TODO Auto-generated catch block
			System.out.println("Webelement is not findable");
		}
	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		String text=ele.getText();
		boolean txt = ele.getText().contains(expectedText);
		try {
			if(txt==true)
			{
				//reportStep("The given text "+ expectedText +" contains actual text "+text, "pass");
			}
			else
			{

				//reportStep("The given text "+ expectedText +" doesnot contains actual text "+text, "fail");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//reportStep("WebElement not found", "fail");
		}
	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		try {
			String attribute2 = ele.getAttribute(attribute);
			if(attribute2.equalsIgnoreCase(value))
			{
			   //reportStep("The given attribute and actual attributes are same", "pass");
			}
			else
			{
				 //reportStep("The given attribute and actual attributes are not same", "fail");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//reportStep("Webelement nof found", "Fail");
		}

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		try {
			String attribute2 = ele.getAttribute(attribute);
			if(attribute2.contains(value))
			{
			   //reportStep("The given attribute and actual attributes are partially verified", "pass");
			}
			else
			{
				 //reportStep("The given attribute and actual attributes are not same", "fail");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//reportStep("Webelement nof found", "Fail");
		}
	}

	@Override
	public void verifySelected(WebElement ele) {
		// TODO Auto-generated method stub
		boolean selected = ele.isSelected();
		try {
			if(selected)
			{
				//reportStep("The given webElement is selected", "pass");
			}
			else
			{
				//reportStep("The element is not selected", "fail");
			}
		} catch (NoSuchElementException e) {
			// TODO Auto-generated catch block
			//reportStep("Element not found", "fail");
		}
	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void switchToWindow(int index) {
		// TODO Auto-generated method stub
		Set<String> number = driver.getWindowHandles();
		System.out.println("Number of windows: "+number.size());
		List<String> tbb=new ArrayList<String>();
		tbb.addAll(number);
		String count = tbb.get(index);
		driver.switchTo().window(count);

	}

	@Override
	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub
		try {
			driver.switchTo().frame(ele);
			//reportStep("switched to given frame", "pass");
		} catch (NoSuchElementException e) {
			// TODO Auto-generated catch block
			//reportStep("please check webelement of the frame", "Fail");
		}
	}

	@Override
	public void acceptAlert() {
		// TODO Auto-generated method stub
		driver.switchTo().alert().accept();

	}

	@Override
	public void dismissAlert() {
		// TODO Auto-generated method stub
		driver.switchTo().alert().dismiss();

	}

	@Override
	public String getAlertText() {
		// TODO Auto-generated method stub
		String text = driver.switchTo().alert().getText();
		//reportStep("The text read from alert is "+text, "pass");
		return null;
	}


	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./snaps/img"+i+".png");
		try {
			FileUtils.copyFile(src, des);
		} catch (IOException e) {
			System.err.println("IOException");
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		driver.close();
	}

	@Override
	public void closeAllBrowsers() {
		// TODO Auto-generated method stub
		driver.quit();
	}

}
