package excel;

import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

	public static Object[][] getExcelData(String filename)throws IOException {
		// TODO Auto-generated method stub
		XSSFWorkbook wbook = new XSSFWorkbook("./data/Clead.xlsx");
		//getSheet
		XSSFSheet sheet = wbook.getSheetAt(0);
		//for Row Count
		int lastRowNum = sheet.getLastRowNum();
		System.out.println(lastRowNum);
		//for Column count of 1st row
		int colcount = sheet.getRow(0).getLastCellNum();
		Object[][] data= new Object[lastRowNum][colcount];
		System.out.println(colcount);
		
		for(int j=1; j<=lastRowNum; j++)
		{
			XSSFRow row= sheet.getRow(j);
			
			for (int k = 0; k < colcount; k++) {
				XSSFCell cell = row.getCell(k);
				String stringCellValue = cell.getStringCellValue();
				data[j-1][k]= stringCellValue;
				System.out.println(stringCellValue);
			}
			}
		return data;
		
		
	}
}
