package testcases1;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import wdMethods.ProjectMethods;
public class TC001_CreateLead extends ProjectMethods {
/*@BeforeClass(groups="common")
public void data()
	{
		testCaseId="TC001_CreateLead";
		testDescription="Create Lead";
		author="Logan";
		category="Smoke";
		
	}*/
@Test(dataProvider="createLead")

public void Step2(String cname,String finame,String laname,String data,String mark,String ph,String mailid)
{
	WebElement crm = locateElement("linkText", "CRM/SFA");
	clickWithNoSnap(crm);
	WebElement cl = locateElement("linkText", "Create Lead");
	click(cl);
	WebElement cmpny = locateElement("id", "createLeadForm_companyName");
	type(cmpny, cname);
	WebElement fname = locateElement("id", "createLeadForm_firstName");
	type(fname, finame);
	WebElement lname = locateElement("id", "createLeadForm_lastName");
	type(lname, laname);
	WebElement srce = locateElement("id", "createLeadForm_dataSourceId");
	selectDropDownUsingText(srce, data);
	WebElement mrk = locateElement("id", "createLeadForm_marketingCampaignId");
	selectDropDownUsingText(mrk, mark);
	WebElement phone = locateElement("id", "createLeadForm_primaryPhoneNumber");
	type(phone, ph);
	WebElement mail = locateElement("id", "createLeadForm_primaryEmail");
	type(mail, mailid);
	WebElement button = locateElement("class", "smallSubmit");
	click(button);
	WebElement fnameVerify = locateElement("id", "viewLead_firstName_sp");
	verifyExactText(fnameVerify, "Logan");
	
	}


@DataProvider(name="createLead")
public Object[][] getdata() {
	Object[][] data=new Object[2][7];
	data[0][0]="Accen";
	data[0][1]="Logan";
	data[0][2]="Ganesh";
	data[0][3]="Conference";
	data[0][4]="Automobile";
	data[0][5]="467474657";
	data[0][6]="Ganesh@gmail.com";
	
	data[1][0]="Google";
	data[1][1]="Santa";
	data[1][2]="Krish";
	data[1][3]="Employee";
	data[1][4]="Road and Track";
	data[1][5]="53678907";
	data[1][6]="santa@gmail.com";
	return data;
}
}