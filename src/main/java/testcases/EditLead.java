package testcases;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class EditLead extends ProjectMethods{
	/*@BeforeClass
	public void data()
	{
		testCaseId="TC002_EditLead";
		testDescription="Edit Lead";
		author="Logan";
		category="Smoke";
		
	}*/
	@Test
	public void step3() throws InterruptedException {
		
		WebElement crm = locateElement("LinkText", "CRM/SFA");
		clickWithNoSnap(crm);
		WebElement fl = locateElement("Xpath", "//a[text()='Leads']");
		click(fl);
		WebElement sl = locateElement("Xpath", "//ul[@class='shortcuts']/li[3]/a");
		click(sl);
		WebElement firname = locateElement("Xpath", "(//input[@name='firstName'])[3]");
		type(firname, "Logan");
		WebElement search = locateElement("Xpath", "//button[contains(text(),'Find Leads')]");
		click(search);
		
		WebElement table=locateElement("Xpath","//table[@class='x-grid3-row-table']");
		List<WebElement> tr=table.findElements(By.tagName("tr"));
		System.out.println(tr.size());
		WebElement firstRow=tr.get(0);
		WebElement txt = locateElement("Xpath", "(//table[@class='x-grid3-row-table']//a)[1]");
		String text = txt.getText();
		System.out.println(text);
		click(txt);	
	    verifyTitle("View Lead | opentaps CRM");
	    WebElement edit = locateElement("Xpath", "//a[text()='Edit']");
		click(edit);
		Thread.sleep(3000);
		WebElement updateCompany = locateElement("Id", "updateLeadForm_companyName");
		updateCompany.clear();
		type(updateCompany, "TestLeaf");
		WebElement submt = locateElement("Xpath", "//input[@value='Update']");
		click(submt);
		Thread.sleep(2000);
	    WebElement verifyCompny = locateElement("Id", "viewLead_companyName_sp");
	    String v=verifyCompny.toString();
	    verifyExactText(verifyCompny, "TestTap");
		
					
	}
}