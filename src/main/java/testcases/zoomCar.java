package testcases;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class zoomCar extends SeMethods {
	
	@Test

	public void login () throws InterruptedException {

		startApp("chrome", "https://www.zoomcar.com/chennai");

		WebElement search = locateElement("class", "search");


		click(search);

		WebElement pickuppoint = locateElement("xpath", "//div[@class ='items'] [1]");

		click(pickuppoint);

		WebElement proceed = locateElement ("class", "proceed");

		click(proceed);

		WebElement day = locateElement("xpath", "//div[@class = 'day'][1]");

		click(day);


		WebElement proceed1 = locateElement("class", "proceed");

		click(proceed1);

		WebElement done = locateElement("class", "proceed");

		click(done);


		List<WebElement> carList = locateElements("class","car-item");
		carList.size();
		System.out.println("The number of Car list is " +carList);


		List<WebElement> priceList = locateElements("class","price");
		List<Integer> num = new ArrayList<>();


		System.out.println(priceList.size());

		for (WebElement eachList : priceList) {

			//	System.out.println(eachList.getText().replaceAll("[^0-9]", ""));

			num.add(Integer.parseInt(eachList.getText().replaceAll("[^0-9]", "")));
		}

		Collections.sort(num);

		System.out.println(num.get(num.size()-1));
		System.out.println();

		Integer l = num.get(num.size()-1);

		System.out.println(l);


		Thread.sleep(2000);
		WebElement carName = locateElement("xpath", ("//div[contains(text(),'"+l+"')]/../../preceding-sibling::div[1]/h3"));
		System.out.println(getText(carName));


		WebElement brandN = locateElement("xpath", "//div[contains(text(),'"+l+"')]/following-sibling::button");
		click(brandN);


	}

	
	}





