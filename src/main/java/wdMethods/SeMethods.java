package wdMethods;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.sound.midi.SysexMessage;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

public class SeMethods implements WdMethods{
	public int i = 1;
	public static RemoteWebDriver driver;
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")){
				
			
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--disable-notifications");

				System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
				
				driver = new ChromeDriver(options);
				
				
			} else if (browser.equalsIgnoreCase("firefox")){
				System.setProperty("webdriver.gecko.driver", "./driver/geckodriver.exe");
				driver = new FirefoxDriver();
			}

			
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			System.out.println("The Browser "+browser+" Launched Successfully");
		} catch (WebDriverException e) {
			System.err.println("The Browser "+browser+" not Launched");
		} finally {
			takeSnap();
		}

	}

	
	public WebElement locateElement(String locator, String locValue) {
		try {
			switch(locator) {
			case "id"	 : return driver.findElementById(locValue);
			case "class" : return driver.findElementByClassName(locValue);
			case "xpath" : return driver.findElementByXPath(locValue);
			case "linkText" : return driver.findElementByLinkText(locValue);
			}
		} catch (NoSuchElementException e) {
			System.err.println("The Element is not found");
		} catch (Exception e) {
			System.err.println("Unknow Exception ");
		}
		return null;
	}

	public List<WebElement> locateElements(String locator, String locValue) {
		try {
			switch(locator) {
			case "id"	 : return driver.findElementsById(locValue);
			case "class" : return driver.findElementsByClassName(locValue);
			case "xpath" : return driver.findElementsByXPath(locValue);
			case "linktext" : return driver.findElementsByLinkText(locValue);
			case "css-selector" : return driver.findElementsByCssSelector(locValue);
			}
		} catch (NoSuchElementException e) {
			System.err.println("The Element is not found");
		} catch (Exception e) {
			System.err.println("Unknow Exception ");
		}
		return null;
	}
	
	@Override
	public WebElement locateElement(String locValue) {
		// TODO Auto-generated method stub
		driver.findElementById(locValue);
		return null;
	}

	
	public void type(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
			System.out.println("The data "+data+" is Entered Successfully");
		} catch (WebDriverException e) {
			System.out.println("The data "+data+" is Not Entered");
		} finally {
			takeSnap();
		}
	}

	
	public void clickWithNoSnap(WebElement ele) {
		try {
			ele.click();
			System.out.println("The Element "+ele+" Clicked Successfully");
		} catch (Exception e) {
			System.err.println("The Element "+ele+"is not Clicked");
		}
	}
	
	
	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			System.out.println("The Element "+ele+" Clicked Successfully");
		} catch (WebDriverException e) {
		System.err.println("The Element "+ele+"is not Clicked");
		} finally {
			takeSnap();
		}
	}

	@Override
	public String getText(WebElement ele) {
		// TODO Auto-generated method stub
		
		String txt = ele.getText();
		System.out.println("Text of the Webelement is" +ele);
		return txt;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select dd = new Select(ele);
			dd.selectByVisibleText(value);
			System.out.println("The DropDown Is Selected with VisibleText "+value);
		} catch (Exception e) {
			System.err.println("The DropDown Is not Selected with VisibleText "+value);
		} finally {
			takeSnap();
		}

	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		// TODO Auto-generated method stub-- by me
		try {
		Select dd = new Select(ele);
		dd.selectByIndex(index);
		System.out.println("The DropDown Is Selected with Index" +index);
		} catch (Exception e) {
			System.out.println("The DropDown Is not Selected with Index" +index);
		} finally {
			takeSnap();
		}
	}
		

	

	@Override
	public boolean verifyTitle(String expectedTitle) {
		// by me//
		WebElement title = driver.findElementByTagName(expectedTitle);
		System.out.println(title.getText());
		return false;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		try {
			String text = ele.getText();
			if(text.equals(expectedText))
			{
				System.out.println("Text matches with the" +expectedText);
			}
			else {
				System.out.println("Text is not matches with the" +expectedText);
			}
		}catch (Exception e){
				System.out.println("Exception in verifyExactText");
			}

		}


	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		try {
			String text = ele.getText();
			if(text.contains(expectedText))
			{
				System.out.println("Text matches with the" +expectedText);
			}
			else {
				System.out.println("Text is not matches with the" +expectedText);
			}
		}catch (Exception e) {
				System.out.println("Excpetion in verifyPartialText");
			}
		}
	

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		try {
             String text= ele.getAttribute(attribute);
             if(text.equals(value))
             {
            	 System.out.println("Attribute value is matched with the" +value);
             }
             else {
            	 System.out.println("Attribute value is not matched with the " +value);
             }
		}catch (Exception e)
		{
			System.out.println("Exception in verifyExactAttribute");
		}

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		try {
		String text=ele.getAttribute(attribute);
		if(text.contains(value))
		{
			System.out.println("Attribute value contains is matched with" +value);
		}
		else {
			System.out.println("Attribute value contains is not matched with" +value);
		}
		} catch (Exception e)
		{
		System.out.println("Excpetion in verifyPartialAttribute");
		}
	}
	
	
	@Override
	public void verifySelected(WebElement ele) {
		// TODO Auto-generated method stub
		boolean selected= ele.isSelected();
		if(selected==true)
		{
			System.out.println(ele+ "is selected");
		}
		else
		{
			System.out.println(ele+ "is not selected");
		}
	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub
		boolean displayed= ele.isDisplayed();
		if(displayed==true)
		{
			System.out.println(ele+ "is displayed");
		}
		else {
			System.out.println(ele+ "is not displayed");
		}
	}

	
	@Override
	public void switchToWindow(int index) {
		// TODO Auto-generated method stub
		Set<String> window = driver.getWindowHandles();
		System.out.println(window.size());
		List<String> window1 = new ArrayList<String>();
		window1.addAll(window);
		String secondwindow = window1.get(1);
		driver.switchTo().window(secondwindow);

	}

	@Override
	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub
		try{
			driver.switchTo().frame(ele);
		} catch(Exception e)
		{
			System.out.println("No such frame Exception" );
		}
		

	}

	@Override
	public void acceptAlert() {
		// TODO Auto-generated method stub
		try{
			
	        driver.switchTo().alert().accept();
		} catch(Exception e)
		{
			System.out.println("No alret Exception");
		}
		

	}

	@Override
	public void dismissAlert() {

		// TODO Auto-generated method stub
		try {
			
			driver.switchTo().alert().dismiss();
		} catch (Exception e)
		{
			System.out.println("Unhandeled Alert Exception");
		}
	}
	
	@Override
	public String getAlertText() {
		// TODO Auto-generated method stub
		try {
		driver.switchTo().alert().getText();
		}
		catch(Exception e)
		{
			System.out.println("Unhandeled Alert Exception");
		}
		return null;
	}

	
	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./snaps/img"+i+".png");
		try {
			FileUtils.copyFile(src, des);
		} catch (IOException e) {
			System.err.println("IOException");
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		driver.close();
	}

	@Override
	public void closeAllBrowsers() {
		// TODO Auto-generated method stub
		driver.quit();

	}
}