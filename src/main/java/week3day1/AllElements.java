package week3day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class AllElements {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver test =new ChromeDriver();
		test.get("http://leaftaps.com/opentaps/");
		
		test.findElementById("username").sendKeys("DemoSalesManager");
		test.findElementById("password").sendKeys("crmsfa");
		test.findElementByClassName("decorativeSubmit").click();
		
		test.findElementByLinkText("CRM/SFA").click();
		test.findElementByLinkText("Create Lead").click();
		test.findElementByClassName("inputBox").sendKeys("Tesla");
		test.findElementById("createLeadForm_firstName").sendKeys("supriya");
		test.findElementById("createLeadForm_lastName").sendKeys("kal");
		//test.findElementById("createLeadForm_parentPartyId").sendKeys("Testleaf");
		
		//drop down//
		
		WebElement d1 =test.findElementById("createLeadForm_dataSourceId");
		Select s1=new Select(d1);
		s1.selectByVisibleText("Conference");
		
		WebElement d2 =test.findElementById("createLeadForm_marketingCampaignId");
		Select s2=new Select(d2);
		s2.selectByValue("CATRQ_AUTOMOBILE");
		
		// fields//
		
		test.findElementById("createLeadForm_firstNameLocal").sendKeys("1stname");
		test.findElementById("createLeadForm_lastNameLocal").sendKeys("1stnamelast");
		test.findElementById("createLeadForm_personalTitle").sendKeys("test");
		test.findElementById("createLeadForm_generalProfTitle").sendKeys("1stprofile");	
		test.findElementById("createLeadForm_departmentName").sendKeys("1stdepartment");
		test.findElementById("createLeadForm_annualRevenue").sendKeys("5.1crore");
		
		
		//drop down//
		
		
		WebElement d3= test.findElementById("createLeadForm_currencyUomId");
		Select s3 = new Select(d3);
		s3.selectByValue("USD");
		
		WebElement d4= test.findElementById("createLeadForm_industryEnumId");
		Select s4 = new Select(d4);
		s4.selectByValue("IND_AEROSPACE");
		
		// field
		test.findElementById("createLeadForm_numberEmployees").sendKeys("500");
		
		//drop down
		
		WebElement d5= test.findElementById("createLeadForm_ownershipEnumId");
		Select s5 = new Select(d5);
		s5.selectByValue("OWN_PROPRIETOR");
		
		// field
		
		test.findElementById("createLeadForm_sicCode").sendKeys("452sc");
		test.findElementById("createLeadForm_tickerSymbol").sendKeys("arrow");
		test.findElementById("createLeadForm_description").sendKeys("testing team creating the request");
		test.findElementById("createLeadForm_importantNote").sendKeys("important notes mentioned over here");
		test.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("2");
		test.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("4576");
		test.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("78945");
		test.findElementById("createLeadForm_primaryEmail").sendKeys("testabc@gmail.com");
		test.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("78946612");
		test.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("tester");
		test.findElementById("createLeadForm_primaryWebUrl").sendKeys("abc.com");
		test.findElementById("createLeadForm_generalToName").sendKeys("devteam");
		test.findElementById("createLeadForm_generalAttnName").sendKeys("qewr");
		test.findElementById("createLeadForm_generalAddress1").sendKeys("add");
		test.findElementById("createLeadForm_generalAddress2").sendKeys("add2");
		test.findElementById("createLeadForm_generalCity").sendKeys("hyd");
		test.findElementById("createLeadForm_generalPostalCode").sendKeys("124563");
		
		//drop down
		WebElement d6= test.findElementById("createLeadForm_generalStateProvinceGeoId");
		Select s6 = new Select(d6);
		s6.selectByValue("AL");	
		
		WebElement d7=test.findElementById("createLeadForm_generalCountryGeoId");
		Select s7 =new Select(d7);
		s7.selectByValue("AFG");
		
		//fields
		
		test.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("45781");
		test.findElementByName("submitButton").click();
		
	}

}
